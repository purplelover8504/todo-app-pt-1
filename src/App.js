import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
    value: "",
  };

  handleAddTodo = (event) => {
    if (event.keyCode === 13) {
      let newtodo = {
        userId: 1,
        id: Math.random() * 10000,
        title: event.target.value,
        completed: false,
      };
      let updatedtodo = [...this.state.todos];
      updatedtodo.push(newtodo);
      this.setState((state) => {
        return { ...state, todos: updatedtodo, value: "" };
      });
    }
  };

  handleChange = (event) => {
    this.setState({ ...this.state, value: event.target.value });
  };

  handleChecked = (id) => {
    let newTodos = this.state.todos.map((todo) => {
      if (todo.id === id) {
        return {
          ...todo,
          completed: !todo.completed,
        };
      }
      return todo;
    });
    this.setState((state) => {
      return {
        ...state,
        todos: newTodos,
      };
    });
  };

  handleDeleteTodo = (event, id) => {
    const newTodoList = this.state.todos.filter((todo) => todo.id !== id);
    this.setState({ todos: newTodoList });
  };

  handleDeleteTodoAll = (event) => {
    const newTodoList = this.state.todos.filter(
      (todo) => todo.completed === false
    );
    this.setState({ todos: newTodoList });
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autofocus
            onKeyDown={this.handleAddTodo}
            value={this.state.value}
            onChange={this.handleChange}
          />
        </header>
        <TodoList
          todos={this.state.todos}
          handleChecked={this.handleChecked}
          handleDeleteTodo={this.handleDeleteTodo}
          handleDeleteTodoAll={this.handleDeleteTodoAll}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button
            className="clear-completed"
            onClick={this.handleDeleteTodoAll}
          >
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onChange={this.props.handleChecked}
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.handleDeleteTodo} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              key={todo.id}
              title={todo.title}
              completed={todo.completed}
              handleChecked={(event) => this.props.handleChecked(todo.id)}
              handleDeleteTodo={(event) =>
                this.props.handleDeleteTodo(event, todo.id)
              }
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
